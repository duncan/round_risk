#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import json
import argparse

import sys
sys.path.append( '..' )

import tools.consts as consts
from tools.topology import Topology, Lane
from tools.exit_tracking import ExitTracking
from tools.flow_measure import EntryFlow
import tools.read_csv as rint
import tools.locations as locations


def get_frames(trackId):
    return (tracks[trackId][rint.INITIAL_FRAME], tracks[trackId][rint.LAST_FRAME])

def read_frames(last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res



# .......................................................................... #
# Browse all recordings associated with a given location to compute TTC data #
# The obtained ttc data is saved into a pickle file to be reused by          #
# analysis.py .............................................................. #
# .......................................................................... #

parser = argparse.ArgumentParser()
parser.add_argument("--location", help="The interaction roundabout name to generate TTC data from.", type=str)
argsparse = parser.parse_args()

print ("Loading recordings for location {}".format(argsparse.location))
input_args = locations.get_input_interaction(argsparse.location)

# Loading topology for the location.
topology = locations.get_topology_interaction(argsparse.location)

# Create the target directory if needed
try:
    os.mkdir('exit_parse')
except FileExistsError:
    pass
    
flow_period = 30.0
framerate = 10.0

for args in input_args:
    tracks, last_frame, last_track = rint.read_track_csv(args)

    print ("Read tracks: {}".format(args))
    print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

    # Loading flow stats
    entry_flow = EntryFlow(topology, framerate)
    entry_flow.read_json('flow_parse/inter_flow_{}_{}.json'.format(argsparse.location, os.path.basename(args)))
    flow_stats = entry_flow.analyze_complete(flow_period)

    print ("Loading frames...")
    frames = read_frames(last_frame=last_frame, last_track=last_track)
    
    # Initialize exit tracking
    exit_data = ExitTracking(topology, flow_stats)

    for frameId in range(len(frames)):
        print("frame {}/{}".format(frameId, last_frame), end='\r')
        for obj in frames[frameId]:
            exit_data.update_vehicle(obj, frameId/framerate)

    result = exit_data.training_data
    with open('exit_parse/inter_exit_{}_{}.json'.format(argsparse.location, os.path.basename(args)), 'w') as handle:
        json.dump(result, handle)
