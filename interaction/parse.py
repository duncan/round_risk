#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle
import argparse

import sys
sys.path.append('tools')

import read_csv as rd
from consts import FRAMERATE_INTERACTION
from topology import Topology, Lane
from ttc_correlation import TTCTimeline, TTCData, VariationDataset

import exit_model as exit_model_lib
import locations


def get_frames(trackId):
    return (tracks[trackId][rd.INITIAL_FRAME], tracks[trackId][rd.LAST_FRAME])

def read_frames(last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][rd.FRAME]).index(frameId)
                pos = tracks[trackId][rd.BBOX][ix]
                res[frameId].append({rd.TRACK_ID: trackId,
                                     rd.X: pos[0], rd.Y: pos[1],
                                     rd.WIDTH: pos[2], rd.HEIGHT: pos[3],
                                     rd.HEADING: tracks[trackId][rd.HEADING][ix],
                                     rd.X_VELOCITY: tracks[trackId][rd.X_VELOCITY][ix],
                                     rd.Y_VELOCITY: tracks[trackId][rd.Y_VELOCITY][ix]})
    return res



# .......................................................................... #
# Browse all recordings associated with a given location to compute TTC data #
# The obtained ttc data is saved into a pickle file to be reused by          #
# analysis.py .............................................................. #
# .......................................................................... #

parser = argparse.ArgumentParser()
parser.add_argument("--dst", help="Directory where the generated pickle files will be saved.", default='ttc_parse')
parser.add_argument("--location", help="The location ID to generate TTC data from.", type=str)
parser.add_argument("--begin", help="Beginning recording ix to generate.", type=int, default=-1)
parser.add_argument("--end", help="Last recording ix to generate.", type=int, default=-1)
parser.add_argument("--probability_weighting", help="Compute risk using vehicles' roundabout exiting probability.", action="store_true")
argsparse = parser.parse_args()

print ("Loading recordings for location {}".format(argsparse.location))

input_str_all = locations.get_input_interaction(argsparse.location)
input_str = input_str_all
if argsparse.begin != -1 and argsparse.end != -1 and argsparse.begin >= 0 and argsparse.end > argsparse.begin and argsparse.end <= len(input_ids):
    input_str = input_str_all[argsparse.begin:argsparse.end]


print ("Input files for location {}: {}".format(argsparse.location, input_str))


# Loading topology for the location.
topology = locations.get_topology_interaction(argsparse.location)

# .................................................................................... #
# If requested, compute exiting probability estimation models to weight TTC risk with. #
# .................................................................................... #


exit_proba_models = {}
if argsparse.probability_weighting:
    print("Computing exit probability models...")
    
    for input_str_val in input_str:
        
        model_training_inputs = []
        for value in input_str:
            if value != input_str_val:
                model_training_inputs.append( argsparse.location+'_'+os.path.basename(value) )
        
        (exit_model, accuracy) = exit_model_lib.get_exit_proba_model(model_training_inputs, 101010)
        print ("Model trained excluding {}_tracks, accuracy: {}".format(os.path.basename(input_str_val), accuracy))
        
        exit_proba_models[input_str_val] = exit_model


# Create the target directory if needed
try:
    os.mkdir(argsparse.dst)
except FileExistsError:
    pass


np.random.seed(42)

# Positioning error simulation
'''position_noises = [0, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
                      0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                      1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
                      2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]'''
position_noises = [0]

for args in input_str:
    tracks, last_frame, last_track = rd.read_track_csv(args)

    print ("Read tracks: {}".format(os.path.basename(args)))
    print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

    print ("Loading frames...")
    frames = read_frames(last_frame=last_frame, last_track=last_track)

    # A. TTC VALUES COMPUTATION
    risk_modes = [1,2,3,4,5,6]
    
    timelines = []
    for noise in position_noises:
        timelines.append( (noise, TTCTimeline(risk_modes, FRAMERATE_INTERACTION)) )

    for frameId in range(len(frames)):
        print("frame {}/{}".format(frameId, last_frame), end='\r')
        for obj in frames[frameId]:
            
            #Fetch TTC data
            ttc = topology.getTTC(frames[frameId], obj, position_noises)
            
            # Compute risk likelihood if requested
            risk_probability = None
            if ttc != None and args in exit_proba_models:
                risk_probability = topology.get_risk_probability(obj, ttc[0], exit_proba_models[args])
            
            for (i, (noise, timeline)) in enumerate(timelines):
                
                #Notify the presence of the object inside the roundabout
                if topology.is_inside_roundabout(obj):
                    timeline.add_occupancy(frameId, obj[rd.TRACK_ID])
                
                #Add TTC data
                if ttc != None and ttc[1][i][1] > 0:
                    
                    if (ttc[1][i][0] != noise):
                        raise ValueError('TTC noise / timeline noise value mismatch')
                    
                    # Add TTC data to the timeline
                    timeline.add(frameId, obj[rd.TRACK_ID], ttc[0][rd.TRACK_ID], ttc[1][i][1], risk_probability)


    result = {'timelines': timelines}
    with open('{}/inter_ttc_{}_{}.pickle'.format(argsparse.dst, argsparse.location, os.path.basename(args)), 'wb') as handle:
        pickle.dump(result, handle)

