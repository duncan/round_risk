#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os, argparse
import numpy as np
import pandas as pd

import sys
sys.path.append( '..' )

import tools.read_csv as rint
import tools.consts as consts
import tools.locations as locations
from tools.topology import Topology, Lane
from tools.flow_measure import EntryFlow


def get_frames(tracks, trackId):
    return (tracks[trackId][rint.INITIAL_FRAME], tracks[trackId][rint.LAST_FRAME])

def read_frames(tracks, last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(tracks, trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res



parser = argparse.ArgumentParser()
parser.add_argument("--location", help="The interaction roundabout name to generate TTC data from.", type=str)
argsparse = parser.parse_args()

print ("Loading recordings for location {}".format(argsparse.location))
input_args = locations.get_input_interaction(argsparse.location)

# Loading topology for the location.
topology = locations.get_topology_interaction(argsparse.location)


# Create the target directory if needed
try:
    os.mkdir('flow_parse')
except FileExistsError:
    pass

for args in input_args:
    tracks, last_frame, last_track = rint.read_track_csv(args)

    print ("Read tracks: {}".format(args))
    print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

    print ("Loading frames...")
    frames = read_frames(tracks, last_frame=last_frame, last_track=last_track)
    
    # Initialize entry flow tracking
    flow = EntryFlow(topology, framerate=10.0)
    flow.load_flow(frames)
    flow.save('flow_parse/inter_flow_{}_{}.json'.format(argsparse.location, os.path.basename(args)))
