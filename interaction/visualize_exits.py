#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle

import sys
sys.path.append('tools')

from consts import ROUND_PATH
import read_csv as rd
from topology import Topology, Lane, get_object_front
from exit_tracking import ExitTracking
from exit_model import get_exit_proba_model, get_exit_probability

# TODO: 1. DEFINE THE PATH TO A TRACKS-FILE REPRESENTING THE WANTED LOCATION HERE
# Note: E.G for a tracks of location ID=0 in my case.
input_file = "09_tracks.csv"
input_meta_file = "09_tracksMeta.csv"
input_recordingMeta_file = "09_recordingMeta.csv"

# .......................... #
# A.1 Loading the tracks file #
# .......................... #
args = {"input_path": ROUND_PATH+input_file, "input_static_path": ROUND_PATH+input_meta_file, "input_meta_path": ROUND_PATH+input_recordingMeta_file}

tracks = rd.read_track_csv(args)
tracks_meta = rd.read_static_info(args)
recordings_meta = rd.read_meta_info(args)

last_frame = int(recordings_meta[rd.FRAME_RATE] * recordings_meta[rd.DURATION]) - 1
last_track = int(recordings_meta[rd.NUM_TRACKS]) - 1

print ("Read tracks: {}".format(input_file))
print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

def read_trajectory(tracks, trackId):
    
    # Trajectory points
    x = []
    y = []
    
    # Browse data
    positions = tracks[trackId][rd.BBOX]
    for pos in positions:
        x.append(pos[0])
        y.append(pos[1])
    
    return (np.array(x), np.array(y))

def get_class(tracks_meta, trackId):
    return tracks_meta[trackId][rd.CLASS]

def get_frames(tracks_meta, trackId):
    return (tracks_meta[trackId][rd.INITIAL_FRAME], tracks_meta[trackId][rd.FINAL_FRAME])

def plot_tracks(tracks_range, draw_lanes, topology):
    
    for trackId in tracks_range:
        trackClass = get_class(tracks_meta, trackId)
        
        (x,y) = read_trajectory(tracks, trackId)
        plt.plot(x,y)
    
    if draw_lanes:
        topology.draw()
    
    plt.title("Tracks for vehicles {}".format(tracks_range))

# ............................................... #
# A.2 Loading the roundabout exit prediction model #
# ............................................... #

input_ids = ['02','03','04','05','06','07','08'] # !! No ID=9:
# => Do not train the model with the used tracks data.

for i in range(10,24):
    input_ids.append('{}'.format(i))

(exit_model, accuracy) = get_exit_proba_model(input_ids, 101010)
print("Exit prediction model accuracy: {}".format(accuracy))

# .................................................................. #
# B. TODO: Use the tracks list to define the lanes topology for each #
# locations ID (Location ID=0 is provided as an example) ........... #
# .................................................................. #

# TODO: 2. DEFINE THE TOPOLOGY OF THE LANES FOR THAT SPECIFIC ROUNDABOUT
topology = Topology.roundDLocation0Topology() # see topology.py


# ................................................................ #
# C. This is a step by step visualzation tool to replay the tracks #
# of each vehicle/object (and verify the position of the circular  #
# lanes) ......................................................... #
# ................................................................ #

def read_frames(last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(tracks_meta, trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][rd.FRAME]).index(frameId)
                pos = tracks[trackId][rd.BBOX][ix]
                res[frameId].append({rd.TRACK_ID: trackId,
                                     rd.X: pos[0], rd.Y: pos[1],
                                     rd.WIDTH: pos[2], rd.HEIGHT: pos[3],
                                     rd.HEADING: tracks[trackId][rd.HEADING][ix],
                                     rd.X_VELOCITY: tracks[trackId][rd.X_VELOCITY][ix],
                                     rd.Y_VELOCITY: tracks[trackId][rd.Y_VELOCITY][ix]})
    return res


print ("Loading frames...")
# 2000 frames only loaded, so that the script launches faster.
# last_frame=last_frame for full visualization
frames = read_frames(last_frame=2000, last_track=last_track)

def draw_object(ax, x, y, width, height, heading):
    rect = patches.Rectangle((x-width/2, y-height/2), width, height, linewidth=1, edgecolor='r', facecolor='r')
    transform = matplotlib.transforms.Affine2D().rotate_deg_around(x, y, heading) + ax.transData
    rect.set_transform(transform)
    return rect



# EXIT TRACKING
fig, ax = plt.subplots()

# NOTE: background image of the roundabout.
# Needs extra work as it must be manually positioned to match the tracks
# of each vehicles
# The provided example shows the background of location ID=0
im = Image.open('loc0_background.png')

for frameId in range(0, len(frames), 10):
    
    plt.title("Frame {}/{}".format(frameId, len(frames)))
    ax.set_xlim(35,125)
    ax.set_ylim(-94,-1)
    
    # NOTE: manual positioning of the background image
    ax.imshow(im, extent=[1,169,-94,-1])
    
    #topology.draw()
    
    for obj in frames[frameId]:
        
        obj_class = get_class(tracks_meta, obj[rd.TRACK_ID])
        if obj_class != "pedestrian" and obj_class != "bicycle":
            
            rect = draw_object(ax, obj[rd.X], obj[rd.Y], obj[rd.WIDTH], obj[rd.HEIGHT], obj[rd.HEADING]+90)
            ax.add_patch(rect)
            
            ttc = topology.getTTC(frames[frameId], obj)
            laneid = topology.get_lane_distance(obj)
            
            if laneid != None:
                signed_relheading = topology.get_relative_heading(obj)
                (_, distance) = topology.get_distance_to_next_exit(obj)
            
                ax.annotate(np.round(get_exit_probability(exit_model, laneid, signed_relheading, distance), 2), (obj[rd.X], obj[rd.Y]), weight='bold', fontsize=12)
            
            '''
            if ttc == None:
                ax.annotate(obj[rd.TRACK_ID], (obj[rd.X], obj[rd.Y]), weight='bold', fontsize=6)
            else:
                
                risk_proba = topology.get_risk_probability(obj, ttc[0], exit_model)
                
                ax.annotate('{}->{}: {}s / proba: {}'.format(obj[rd.TRACK_ID], ttc[0][rd.TRACK_ID], np.round(ttc[1], 2), np.round(risk_proba, 2)), (obj[rd.X], obj[rd.Y]), weight='bold', fontsize=6)
            
            #ax.annotate(obj[rd.TRACK_ID], (obj[rd.X]+2, obj[rd.Y]+2), weight='bold', fontsize=6)
        
            tangential_angle = topology.gettangentialangle(obj)
            tangent = np.deg2rad(tangential_angle)
            heading = np.deg2rad(obj[rd.HEADING])
            obj_pos = get_object_front(obj)
            plt.plot([obj_pos[0], obj_pos[0]+5*np.cos(tangent)], [obj_pos[1], obj_pos[1]+5*np.sin(tangent)], color="blue")
            plt.plot([obj_pos[0], obj_pos[0]+5*np.cos(heading)], [obj_pos[1], obj_pos[1]+5*np.sin(heading)], color="red")
            #ax.annotate(np.round(topology.get_relative_heading(obj),1), (obj[rd.X]+2, obj[rd.Y]+2), weight='bold', fontsize=6)
            '''
        
        '''
        
        '''
    
    plt.waitforbuttonpress()
    plt.cla()
