#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.lines import Line2D
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle

import sys
sys.path.append('..')

import tools.read_csv as inter
import tools.consts as consts
from tools.topology import Topology, Lane, get_object_front
from tools.ttc_correlation import TTCTimeline, TTCData, VariationDataset
from tools.flow_measure import EntryFlow

# TODO: 1. DEFINE THE PATH TO A TRACKS-FILE REPRESENTING THE WANTED LOCATION HERE
# Note: E.G for a tracks of location ID=0 in my case.

input_file = consts.INTER_PATH + "/DR_USA_Roundabout_FT/vehicle_tracks_000.csv"
tracks, last_frame, last_track = inter.read_track_csv(input_file)
print ("Read tracks: {}".format(input_file))
print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

def read_trajectory(tracks, trackId):
        
        # Trajectory points
        x = []
        y = []
        
        # Browse data
        positions = tracks[trackId][consts.BBOX]
        for pos in positions:
            x.append(pos[0])
            y.append(pos[1])
        
        return (np.array(x), np.array(y))

def get_frames(trackId):
    return (tracks[trackId][inter.INITIAL_FRAME], tracks[trackId][inter.LAST_FRAME])

def plot_tracks(tracks_range, draw_lanes, topology):
    
    for trackId in tracks_range:
        (x,y) = read_trajectory(tracks, trackId)
        plt.plot(x,y,'grey', alpha=0.3)
    
    if draw_lanes:
        topology.draw()
    
    plt.title("Tracks for vehicles {}".format(tracks_range))

'''
for i in range(0,5):
    input_file = ""
    if i < 10:
        input_file = consts.INTER_PATH + "/DR_CHN_Roundabout_LN/vehicle_tracks_00{}.csv".format(i)
    else:
        input_file = consts.INTER_PATH + "/DR_CHN_Roundabout_LN/vehicle_tracks_0{}.csv".format(i)

    # .......................... #
    # A. Loading the tracks file #
    # .......................... #

    tracks, last_frame, last_track = inter.read_track_csv(input_file)
    print ("Read tracks: {}".format(input_file))
    print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))


    # .................................................................. #
    # B. TODO: Use the tracks list to define the lanes topology for each #
    # locations ID (Location ID=0 is provided as an example) ........... #
    # .................................................................. #

    # TODO: 2. DEFINE THE TOPOLOGY OF THE LANES FOR THAT SPECIFIC ROUNDABOUT
    

    # Plot all the tracks trajectories from the given tracks file
    plot_tracks(range(0,last_track), False, None)
'''
topology = Topology.interaction_USA_FT_Topology() # see topology.py
#topology.draw(all_lanes=False)

custom_lines = [Line2D([0], [0], color='green', lw=4),
                Line2D([0], [0], color='blue', lw=4),
                Line2D([0], [0], color='red', lw=4),
                Line2D([0], [0], color='black', lw=2),
                Line2D([0], [0], color='grey', lw=2)]
'''
plt.title('Flow Sensors in the DR_CHN_Roundabout_LN Roundabout')
plt.legend(custom_lines, ['Entry Sensor', 'Upstream Entry Sensor', 'Exit Sensor', 'Circular Part', 'Vehicle Tracks'])
plt.xlabel('x-axis coordinate (m)')
plt.ylabel('y-axis coordinate (m)')
plt.show()
'''


# ................................................................ #
# C. This is a step by step visualzation tool to replay the tracks #
# of each vehicle/object (and verify the position of the circular  #
# lanes) ......................................................... #
# ................................................................ #

def read_frames(last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res


print ("Loading frames...")
# 2000 frames only loaded, so that the script launches faster.
# last_frame=last_frame for full visualization
frames = read_frames(last_frame=3000, last_track=last_track)


def draw_object(ax, x, y, width, height, heading):
    rect = patches.Rectangle((x-width/2, y-height/2), width, height, linewidth=1, edgecolor='r', facecolor='r')
    transform = matplotlib.transforms.Affine2D().rotate_deg_around(x, y, heading) + ax.transData
    rect.set_transform(transform)
    return rect


fig, ax = plt.subplots()

# NOTE: background image of the roundabout.
# Needs extra work as it must be manually positioned to match the tracks
# of each vehicles
# The provided example shows the background of location ID=0

for frameId in range(0, len(frames), 10):
    
    plt.title("Frame {}/{}".format(frameId, len(frames)))
    ax.set_xlim(950,1060)
    ax.set_ylim(940,1060)
    
    
    topology.draw()
    
    for obj in frames[frameId]:
        rect = draw_object(ax, obj[consts.X], obj[consts.Y], obj[consts.WIDTH], obj[consts.HEIGHT], obj[consts.HEADING]+90)
        ax.add_patch(rect)
        
        front_x, front_y = get_object_front(obj)
        
        ax.scatter([front_x], [front_y])
        
        ttc = topology.getTTC(frames[frameId], obj, [0.0])
        if ttc == None:
            ax.annotate(obj[consts.TRACK_ID], (obj[consts.X], obj[consts.Y]), weight='bold', fontsize=6)
        else:
            ttc_val = ttc[1][0][1]
            ax.annotate('{}->{}: {}s'.format(obj[consts.TRACK_ID], ttc[0][consts.TRACK_ID], np.round(ttc_val, 2)), (obj[consts.X], obj[consts.Y]), weight='bold', fontsize=6)
    
    plt.waitforbuttonpress()
    plt.cla()

