#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import argparse

import tools.read_csv_round as rd
import tools.consts as consts
from tools.topology import Topology
from tools.exit_model import get_exit_proba_model, get_exit_probability
from tools.locations import get_input_for_location

# 1. Read command line parameters
parser = argparse.ArgumentParser()
parser.add_argument("--round", help="Path to the root directory of the rounD dataset (the 'data' folder, containing the recordings).", required=True)
parser.add_argument("--recording", help="The identifier of the RounD recording to use (example values: '05', '18'). Default: 09", default='09')
parser.add_argument("-t", help="Set this value to show the topology details", action='store', nargs='*')
argsparse = parser.parse_args()

ROUND_PATH = argsparse.round
track_file_id = argsparse.recording
draw_topology = (argsparse.t != None)

# .......................... #
# A.1 Loading the tracks file #
# .......................... #

input_file = "{}_tracks.csv".format(argsparse.recording)
input_meta_file = "{}_tracksMeta.csv".format(argsparse.recording)
input_recordingMeta_file = "{}_recordingMeta.csv".format(argsparse.recording)

args = {"input_path": ROUND_PATH+'/'+input_file, "input_static_path": ROUND_PATH+'/'+input_meta_file, "input_meta_path": ROUND_PATH+'/'+input_recordingMeta_file}

tracks = rd.read_track_csv(args)
tracks_meta = rd.read_static_info(args)
recordings_meta = rd.read_meta_info(args)

last_frame = int(recordings_meta[rd.FRAME_RATE] * recordings_meta[rd.DURATION]) - 1
last_track = int(recordings_meta[rd.NUM_TRACKS]) - 1

print ("Read tracks: {}".format(input_file))
print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))


def get_class(tracks_meta, trackId):
    return tracks_meta[trackId][rd.CLASS]

def get_frames(tracks_meta, trackId):
    return (tracks_meta[trackId][rd.INITIAL_FRAME], tracks_meta[trackId][rd.FINAL_FRAME])

# ............................................................................ #
# A.2 Train the roundabout exit prediction model on data from other recordings #
# ............................................................................ #

input_ids = get_input_for_location(0)
input_ids.remove(track_file_id)

(exit_model, accuracy) = get_exit_proba_model(input_ids, 101010, interaction=False)
print("Exit prediction model accuracy: {}".format(accuracy))

# ..................................................................................... #
# B. Load the topology for the considered roundabout (in this script, RounD location 0) #
# ..................................................................................... #
topology = Topology.roundDLocation0Topology() # see topology.py


# ................................................................ #
# C. This is a step by step visualzation tool to replay the tracks #
# of each vehicle/object (and verify the position of the circular  #
# lanes) ......................................................... #
# ................................................................ #

def read_frames(last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(tracks_meta, trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res


print ("Loading frames...")

# 2000 frames only loaded, so that the script launches faster.
# last_frame=last_frame for full visualization
frames = read_frames(last_frame=2000, last_track=last_track)

def draw_object(ax, x, y, width, height, heading):
    rect = patches.Rectangle((x-width/2, y-height/2), width, height, linewidth=1, edgecolor='r', facecolor='r')
    transform = matplotlib.transforms.Affine2D().rotate_deg_around(x, y, heading) + ax.transData
    rect.set_transform(transform)
    return rect


# EXIT TRACKING
fig, ax = plt.subplots()

# NOTE: background image of the roundabout.
# Needs extra work as it must be manually positioned to match the tracks
# of each vehicles
# The provided example shows the background of location ID=0
im = Image.open('loc0_background.png')

for frameId in range(0, len(frames), 10):
    
    plt.title("Frame {}/{}".format(frameId, len(frames)))
    ax.set_xlim(35,125)
    ax.set_ylim(-94,-1)
    
    # NOTE: manual positioning of the background image
    ax.imshow(im, extent=[1,169,-94,-1])
    
    if draw_topology:
        topology.draw()
    
    for obj in frames[frameId]:
        
        obj_class = get_class(tracks_meta, obj[consts.TRACK_ID])
        if obj_class != "pedestrian" and obj_class != "bicycle":
            
            rect = draw_object(ax, obj[consts.X], obj[consts.Y], obj[consts.WIDTH], obj[consts.HEIGHT], obj[consts.HEADING]+90)
            ax.add_patch(rect)
            
            laneid = topology.get_lane_distance(obj)
            if laneid != None:
                signed_relheading = topology.get_relative_heading(obj)
                (nextexitid, distance, _) = topology.get_distance_to_next_exit(obj)
                exit_xy = topology.exit_points_cartesian[nextexitid]
                
                plt.arrow(obj[consts.X], obj[consts.Y], exit_xy[0]-obj[consts.X], exit_xy[1]-obj[consts.Y], color='white', width=0.5)
                ax.annotate(np.round(get_exit_probability(exit_model, laneid, signed_relheading, distance), 2),
                            (obj[consts.X], obj[consts.Y]), weight='bold', fontsize=10, bbox=dict(boxstyle='square,pad=-0.05', fc='white', ec='none'))
            
            
    plt.waitforbuttonpress()
    plt.cla()
