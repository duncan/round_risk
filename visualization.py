#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle

import tools.locations as locations
import tools.read_csv_round as rd
import tools.consts as consts
from tools.topology import Topology, Lane
from tools.ttc_correlation import TTCTimeline, TTCData, VariationDataset
from tools.flow_measure import EntryFlow

# TODO: 1. DEFINE THE PATH TO A TRACKS-FILE REPRESENTING THE WANTED LOCATION HERE
# Note: E.G for a tracks of location ID=0 in my case.
location = 0
id_file = "02"

input_file = "{}_tracks.csv".format(id_file)
input_meta_file = "{}_tracksMeta.csv".format(id_file)
input_recordingMeta_file = "{}_recordingMeta.csv".format(id_file)

# .......................... #
# A. Loading the tracks file #
# .......................... #
args = {"input_path": consts.ROUND_PATH+input_file, "input_static_path": consts.ROUND_PATH+input_meta_file, "input_meta_path": consts.ROUND_PATH+input_recordingMeta_file}

tracks = rd.read_track_csv(args)
tracks_meta = rd.read_static_info(args)
recordings_meta = rd.read_meta_info(args)

last_frame = int(recordings_meta[rd.FRAME_RATE] * recordings_meta[rd.DURATION]) - 1
last_track = int(recordings_meta[rd.NUM_TRACKS]) - 1

print ("Read tracks: {}".format(input_file))
print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

def read_trajectory(tracks, trackId):
    
    # Trajectory points
    x = []
    y = []
    
    # Browse data
    positions = tracks[trackId][consts.BBOX]
    for pos in positions:
        x.append(pos[0])
        y.append(pos[1])
    
    return (np.array(x), np.array(y))

def get_class(tracks_meta, trackId):
    return tracks_meta[trackId][rd.CLASS]

def get_frames(tracks_meta, trackId):
    return (tracks_meta[trackId][rd.INITIAL_FRAME], tracks_meta[trackId][rd.FINAL_FRAME])

def plot_tracks(tracks, tracks_range, draw_lanes, topology):
    
    tracks_counted = 0
    for trackId in tracks_range:
        if get_class(tracks_meta, trackId) == 'pedestrian' or get_class(tracks_meta, trackId) == 'bicycle':
            continue
        (x,y) = read_trajectory(tracks, trackId)
        plt.plot(x,y)
        tracks_counted += 1
    
    if draw_lanes:
        topology.draw()
    
    print (tracks_counted)
    #plt.title("Tracks for vehicles {}".format(tracks_range))


# .................................................................. #
# B. TODO: Use the tracks list to define the lanes topology for each #
# locations ID (Location ID=0 is provided as an example) ........... #
# .................................................................. #

# TODO: 2. DEFINE THE TOPOLOGY OF THE LANES FOR THAT SPECIFIC ROUNDABOUT
topology = Topology.roundDLocation0Topology() # see topology.py


'''
# Plot all the tracks trajectories from the given tracks file
input_ids = locations.get_input_for_location(location)
for input_id in input_ids:
    args = {"id": input_id, "input_path": consts.ROUND_PATH+input_id+"_tracks.csv",
                       "input_static_path": consts.ROUND_PATH+input_id+"_tracksMeta.csv",
                       "input_meta_path": consts.ROUND_PATH+input_id+"_recordingMeta.csv" }
    
    tracks = rd.read_track_csv(args)
    tracks_meta = rd.read_static_info(args)
    recordings_meta = rd.read_meta_info(args)

    last_frame = int(recordings_meta[rd.FRAME_RATE] * recordings_meta[rd.DURATION]) - 1
    last_track = int(recordings_meta[rd.NUM_TRACKS]) - 1

    print ("Read tracks: {}".format(args['input_path']))
    print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))
    plot_tracks(tracks, range(0, last_track+1), False, topology)

topology.draw()
plt.title('Overview of all Available Vehicle Trajectories for RounD_2')
plt.show()
'''

# ................................................................ #
# C. This is a step by step visualzation tool to replay the tracks #
# of each vehicle/object (and verify the position of the circular  #
# lanes) ......................................................... #
# ................................................................ #

def read_frames(last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(tracks_meta, trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res


print ("Loading frames...")
# 2000 frames only loaded, so that the script launches faster.
# last_frame=last_frame for full visualization
frames = read_frames(last_frame=3000, last_track=last_track)

def draw_object(ax, x, y, width, height, heading):
    rect = patches.Rectangle((x-width/2, y-height/2), width, height, linewidth=1, edgecolor='r', facecolor='r')
    transform = matplotlib.transforms.Affine2D().rotate_deg_around(x, y, heading) + ax.transData
    rect.set_transform(transform)
    return rect


fig, ax = plt.subplots()

# NOTE: background image of the roundabout.
# Needs extra work as it must be manually positioned to match the tracks
# of each vehicles
# The provided example shows the background of location ID=0
im = Image.open('loc0_background.png')

for frameId in range(0, len(frames), 5):
    
    plt.title("Frame {}/{} ({}s)".format(frameId, len(frames), frameId/25.0))
    ax.set_xlim(35,125) #round_0 (with img)
    ax.set_ylim(-94,-1) #round_0 (with img)
    #ax.set_xlim(80,150) #round_1
    #ax.set_ylim(-110,-30) #round_1
    
    
    # NOTE: manual positioning of the background image
    ax.imshow(im, extent=[1,169,-94,-1])
    
    topology.draw()
    
    for obj in frames[frameId]:
        rect = draw_object(ax, obj[consts.X], obj[consts.Y], obj[consts.WIDTH], obj[consts.HEIGHT], obj[consts.HEADING]+90)
        ax.add_patch(rect)
        
        ttc = topology.getTTC(frames[frameId], obj, [0])
        
        if ttc == None or ttc[1][0][1] < 0:
            ax.annotate(obj[consts.TRACK_ID], (obj[consts.X], obj[consts.Y]), weight='bold', fontsize=12)
        else:
            ax.annotate('{}->{} TTC={}s'.format(obj[consts.TRACK_ID], ttc[0][consts.TRACK_ID], np.round(ttc[1][0][1], 2)), (obj[consts.X], obj[consts.Y]), weight='bold', fontsize=12)
    
    plt.waitforbuttonpress()
    plt.cla()

