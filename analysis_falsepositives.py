#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle
import argparse
import json

import tools.read_csv_round as rd
import tools.consts as consts
from tools.topology import Topology, Lane
from tools.ttc_correlation import TTCTimeline, TTCData, VariationDataset
import tools.exit_model as exit_model_lib
import tools.locations as locations



ttc_threshold = 3.0
framerate = 25.0

target_recording = '02'
input_ids = locations.get_input_for_location(0)

topology = locations.get_topology_for_location(0)

exit_proba_models = {}
print("Computing the exit probability model...")


model_training_inputs = input_ids.copy()
model_training_inputs.remove(target_recording) #Do not train the model using the data it will be applied to.
print (model_training_inputs)
    
(exit_model, accuracy) = exit_model_lib.get_exit_proba_model(model_training_inputs, 101010, interaction=False)
print ("Model trained excluding {}_tracks, accuracy: {}".format(target_recording, accuracy))




dataset = pd.read_csv('false_positives/{}.csv'.format(target_recording))
dataset = dataset[dataset['exit_rear'] != dataset['exit_front']]
dataset = dataset[dataset['ttc'] < ttc_threshold]
#dataset = dataset[dataset['real_rear_exit'] == dataset['exit_rear']] #false positives analysis
dataset = dataset[dataset['real_rear_exit'] != dataset['exit_rear']] #true positives analysis
print (dataset)


#Risk computation
total_risk_tet = 0.0
total_risk_proba = 0.0


for index, row in dataset.iterrows():
    stay_proba = 1.0 - exit_model_lib.get_exit_probability(exit_model, row['lateral_pos_rear'], row['heading_rear'], row['distance_rear'])
    #print ("{} : stay proba {}".format(row, stay_proba))
    
    total_risk_tet += 1.0/framerate
    total_risk_proba += stay_proba/framerate

print ("tet: {}s, proba: {}s / ratio:{} / reduction:{}%".format(total_risk_tet,total_risk_proba, total_risk_tet/total_risk_proba, (total_risk_tet-total_risk_proba)/total_risk_tet*100.0))
