#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle
import argparse
import json

import tools.read_csv_round as rd
import tools.consts as consts
from tools.topology import Topology, Lane
from tools.ttc_correlation import TTCTimeline, TTCData, VariationDataset
import tools.exit_model as exit_model_lib
import tools.locations as locations


def get_frames(tracks_meta, trackId):
    return (tracks_meta[trackId][rd.INITIAL_FRAME], tracks_meta[trackId][rd.FINAL_FRAME])

def read_frames(tracks, tracks_meta, last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(tracks_meta, trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res



# .......................................................................... #
# Browse all recordings associated with a given location to compute TTC data #
# The obtained ttc data is saved into a pickle file to be reused by          #
# analysis.py .............................................................. #
# .......................................................................... #

parser = argparse.ArgumentParser()
parser.add_argument("--dst", help="Directory where the generated pickle files will be saved.", default='ttc_parse')
parser.add_argument("--location", help="The location ID to generate TTC data from.", type=int)
parser.add_argument("--begin", help="Beginning recording ix to generate.", type=int, default=-1)
parser.add_argument("--end", help="Last recording ix to generate.", type=int, default=-1)
parser.add_argument("--probability_weighting", help="Compute risk using vehicles' roundabout exiting probability.", action="store_true")
argsparse = parser.parse_args()

print ("Loading recordings for location {}".format(argsparse.location))

input_ids_all = locations.get_input_for_location(argsparse.location)
input_ids = input_ids_all
if argsparse.begin != -1 and argsparse.end != -1 and argsparse.begin >= 0 and argsparse.end > argsparse.begin and argsparse.end <= len(input_ids):
    input_ids = input_ids_all[argsparse.begin:argsparse.end]

print ("Input files for location {}: {}".format(argsparse.location, input_ids))


input_args = []

for input_id in input_ids:
    input_args.append({"id": input_id, "input_path": consts.ROUND_PATH+input_id+"_tracks.csv",
                       "input_static_path": consts.ROUND_PATH+input_id+"_tracksMeta.csv",
                       "input_meta_path": consts.ROUND_PATH+input_id+"_recordingMeta.csv" })


# Loading topology for the location.
topology = locations.get_topology_for_location(argsparse.location)

# .................................................................................... #
# If requested, compute exiting probability estimation models to weight TTC risk with. #
# .................................................................................... #

exit_proba_models = {}
if argsparse.probability_weighting:
    print("Computing exit probability models...")
    
    for input_str in input_ids:
        model_training_inputs = input_ids_all.copy()
        model_training_inputs.remove(input_str) #Do not train the model using the data it will be applied to.
        
        (exit_model, accuracy) = exit_model_lib.get_exit_proba_model(model_training_inputs, 101010, interaction=False)
        print ("Model trained excluding {}_tracks, accuracy: {}".format(input_str, accuracy))
        print ('{} -> {}'.format(input_str, model_training_inputs))
        
        exit_proba_models[input_str] = exit_model


# Create the target directory if needed
try:
    os.mkdir(argsparse.dst)
except FileExistsError:
    pass


np.random.seed(42)

# Positioning error simulation
position_noises = [0, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
                      0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
                      1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
                      2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]

for args in input_args:
    tracks = rd.read_track_csv(args)
    tracks_meta = rd.read_static_info(args)
    recordings_meta = rd.read_meta_info(args)

    last_frame = int(recordings_meta[rd.FRAME_RATE] * recordings_meta[rd.DURATION]) - 1
    last_track = int(recordings_meta[rd.NUM_TRACKS]) - 1

    print ("Read tracks: {}".format(args['input_path']))
    print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

    print ("Loading frames...")
    frames = read_frames(tracks, tracks_meta, last_frame=last_frame, last_track=last_track)

    # A. TTC VALUES COMPUTATION
    risk_modes = [1,2,3,4,5,6]
    
    timelines = []
    for noise in position_noises:
        timelines.append( (noise, TTCTimeline(risk_modes, recordings_meta[rd.FRAME_RATE])) )

    for frameId in range(len(frames)):
        print("frame {}/{}".format(frameId, last_frame), end='\r')
        for obj in frames[frameId]:
            
            #Fetch TTC data
            ttc = topology.getTTC(frames[frameId], obj, position_noises)
            
            # Compute risk likelihood if requested
            risk_probability = None
            if ttc != None and args['id'] in exit_proba_models:
                risk_probability = topology.get_risk_probability(obj, ttc[0], exit_proba_models[args['id']])
            
            for (i, (noise, timeline)) in enumerate(timelines):
                
                #Notify the presence of the object inside the roundabout
                if topology.is_inside_roundabout(obj):
                    timeline.add_occupancy(frameId, obj[consts.TRACK_ID])
                
                #Add TTC data
                if ttc != None and ttc[1][i][1] > 0:
                    
                    if (ttc[1][i][0] != noise):
                        raise ValueError('TTC noise / timeline noise value mismatch')
                    
                    # Add TTC data to the timeline
                    timeline.add(frameId, obj[consts.TRACK_ID], ttc[0][consts.TRACK_ID], ttc[1][i][1], risk_probability)

    tl_dicts = []
    for tl in timelines:
        tl_dicts.append((tl[0], tl[1].export_dict()))
    
    with open('{}/round_ttc_{}.json'.format(argsparse.dst, args['id']), 'w') as handle:
        json.dump(tl_dicts, handle)

