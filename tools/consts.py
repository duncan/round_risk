#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

# Constants
ROUND_PATH = "/home/scooter/round_risk/path/to/round/data/" # TODO: Replace with the path to the RounD dataset
INTER_PATH = "/home/scooter/round_risk/interaction/interaction/recorded_trackfiles"

FRAMERATE_INTERACTION = 10

# Object parameters
BBOX = "bbox"
FRAME = "frame_id"
TRACK_ID = "track_id"
HEADING = "psi_rad"
X_VELOCITY = "vx"
Y_VELOCITY = "vy"
X = "x"
Y = "y"
WIDTH = "width"
HEIGHT = "length"
