#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import json
import pandas as pd
import numpy as np
import tools.consts as consts

class EntryFlow:
    def __init__(self, topology, framerate):
        self.topology = topology
        self.framerate = framerate
        
        self.stats = pd.DataFrame({'FrameID':[], 'Time':[], 'vid':[], 'EntryPoint':[]})
        self.already_entered = {}
        
        self.circular_occupancy = pd.DataFrame({'FrameID':[], 'Time':[], 'vid':[]})
        
        self.stats_circular = pd.DataFrame({'FrameID':[], 'Time':[], 'vid':[], 'CircularPoint':[]})
        self.entered_circular = {}
    
    def report_entry(self, obj, entry_point, frameID):
        vid = obj[consts.TRACK_ID]
        if entry_point >= 0 and entry_point < len(self.topology.entry_lanescount):
            
            # For Traffic Volume
            if vid not in self.already_entered:
                self.already_entered[vid] = {'fbegin': frameID, 'pbegin': (obj[consts.X],obj[consts.Y])}
                new_row = {'FrameID':frameID, 'Time': frameID/self.framerate, 'vid':vid, 'EntryPoint':entry_point}
                self.stats = self.stats.append(new_row, ignore_index=True)
                print ('Object {} enters at frame {} ({}s) at entry {}'.format(vid,frameID,frameID/self.framerate,entry_point))
            
            # For Waiting Time
            if vid in self.already_entered:
                self.already_entered[vid]['fend'] = frameID
                self.already_entered[vid]['pend'] = (obj[consts.X],obj[consts.Y])
    
    def report_circular_entry(self, obj, circular_point, frameID):
        vid = obj[consts.TRACK_ID]
        if circular_point >= 0 and circular_point < len(self.topology.circular_sensors):
            
            if circular_point not in self.entered_circular:
                self.entered_circular[circular_point] = []
            
            if vid not in self.entered_circular[circular_point]:
                self.entered_circular[circular_point].append(vid)
                new_row = {'FrameID':frameID, 'Time': frameID/self.framerate, 'vid':vid, 'CircularPoint':circular_point}
                self.stats_circular = self.stats_circular.append(new_row, ignore_index=True)
                print ('Object {} circular at frame {} ({}s) at circular {}'.format(vid,frameID,frameID/self.framerate,circular_point))
            
    
    def load_flow(self, frames):
        for frameId in range(len(frames)):
            for obj in frames[frameId]:
                # Entry statistics
                entry_point = self.topology.getobjectenters(obj)
                if entry_point != -1:
                    self.report_entry(obj, entry_point, frameId)
                
                # Circular density statistics
                if self.topology.is_inside_roundabout(obj):
                    new_row = {'FrameID':frameId, 'Time': frameId/self.framerate, 'vid':obj[consts.TRACK_ID]}
                    self.circular_occupancy = self.circular_occupancy.append(new_row, ignore_index=True)
                
                # Circular flow statistics
                circular_point = self.topology.getobject_incircular(obj)
                if circular_point != -1:
                    self.report_circular_entry(obj, circular_point, frameId)
    
    def save(self, target):
        data = {'framerate': self.framerate, 'stats': self.stats.to_json(), 'entered': self.already_entered, 'circular_occupancy': self.circular_occupancy.to_json(),
                'circular_stats':self.stats_circular.to_json(), 'circular_entered': self.entered_circular}
        with open(target, 'w') as handle:
            json.dump(data, handle)
    
    
    def read_json(self, path):
        with open(path) as json_file:
            data = json.load(json_file)
            self.framerate = data['framerate']
            self.stats = pd.read_json(data['stats'])
            self.circular_occupancy = pd.read_json(data['circular_occupancy'])
            self.already_entered = data['entered']
            self.stats_circular = pd.read_json(data['circular_stats'])
            self.entered_circular = data['circular_entered']
            
        
    def analyze(self, timestep):
        i = 0
        time0 = 0.0
        summary = pd.DataFrame({'TimeInterval':[], 'TimeBegin':[], 'TimeEnd':[], 'MeanApproachSpeed':[], 'VolumePerLane':[], 'FlowPerLane':[], 'Flow':[], 'MeanOccupancy':[], 'MeanDensity':[]})
        while time0 < self.stats['Time'].max():
            time1 = time0 + timestep
            
            # Entry Stats
            sub_df = self.stats[(self.stats['Time'] >= time0) & (self.stats['Time'] < time1)]
            
            flow = (sub_df.shape[0] / timestep) * 3600.0
            
            vids = list(sub_df['vid'])
            speeds = []
            for vid in vids:
                vid = '{}'.format(vid)
                distance = np.linalg.norm(np.array(self.already_entered[vid]['pbegin'])-np.array(self.already_entered[vid]['pend']))
                duration = (self.already_entered[vid]['fend'] - self.already_entered[vid]['fbegin']) / self.framerate
                if duration > 0:
                    speeds.append(distance/duration)
            speeds = np.array(speeds)
            
            sub_circular = self.circular_occupancy[(self.circular_occupancy['Time'] >= time0) & (self.circular_occupancy['Time'] < time1)]
            
            mean_speeds = None
            if len(speeds) > 0:
                mean_speeds = np.mean(speeds)
            
            mean_occupancy = 0
            if sub_circular.shape[0] > 0:
                mean_occupancy = sub_circular.groupby('FrameID')['vid'].count().mean()
            
            # Circular Occupancy Stats
            new_row = {'TimeInterval':i,
                       'TimeBegin':time0,
                       'TimeEnd':time1,
                       'MeanApproachSpeed': mean_speeds,
                       'VolumePerLane':sub_df.shape[0]/np.sum(self.topology.entry_lanescount),
                       'FlowPerLane':flow / np.sum(self.topology.entry_lanescount), #veh/h / lane
                       'Flow':flow,
                       'MeanOccupancy':mean_occupancy,
                       'MeanDensity':mean_occupancy/self.topology.available_length()}
            
            summary = summary.append(new_row, ignore_index=True)
            
            time0 = time1
            i += 1
            
        return summary
    
    
    def analyze_circular(self, timestep):
        i = 0
        time0 = 0.0
        
        cols = {'TimeInterval':[], 'TimeBegin':[], 'TimeEnd':[]}
        for circular_point in range(len(self.topology.circular_sensors)):
            cols['Volume{}'.format(circular_point)] = []
            cols['Flow{}'.format(circular_point)] = []
        
        summary = pd.DataFrame(cols)
        while time0 < self.stats_circular['Time'].max():
            time1 = time0 + timestep
            
            # Entry Stats
            sub_df = self.stats_circular[(self.stats_circular['Time'] >= time0) & (self.stats_circular['Time'] < time1)]
            volume_by_point = sub_df.groupby('CircularPoint')['vid'].count()
            
            new_row = {'TimeInterval':i,
                       'TimeBegin':time0,
                       'TimeEnd':time1}
            
            for circular_point in range(len(self.topology.circular_sensors)):
                try:
                    new_row['Volume{}'.format(circular_point)] = volume_by_point.loc[circular_point]
                    new_row['Flow{}'.format(circular_point)] = (volume_by_point.loc[circular_point] / timestep) * 3600.0 #veh/h
                except:
                    new_row['Volume{}'.format(circular_point)] = 0.0
                    new_row['Flow{}'.format(circular_point)] = 0.0
            
            new_row['Capacity_HCM2016'] = EntryFlow.rd_capacity_hcm2016(self.topology, new_row)
            new_row['Capacity_German'] = EntryFlow.rd_capacity_german(self.topology, new_row)
            
            print ('capacity 2010: {} / 2016: {} / german: {}'.format(EntryFlow.rd_capacity_hcm2010_(self.topology, new_row), new_row['Capacity_HCM2016'], new_row['Capacity_German']))
            
            summary = summary.append(new_row, ignore_index=True)
            
            time0 = time1
            i += 1
        
        return summary
    
    def analyze_complete(self, timestep):
        res_entry = self.analyze(timestep)
        res_circular = self.analyze_circular(timestep)
        
        summary = pd.concat([res_entry,res_circular[['Capacity_German', 'Capacity_HCM2016']]], axis=1)
        summary['FlowOverCapacity_German'] = summary['Flow'] / summary['Capacity_German']
        summary['FlowOverCapacity_HCM2016'] = summary['Flow'] / summary['Capacity_HCM2016']
        
        return summary
    
    
    @staticmethod
    def rd_capacity_hcm2010_(topology, circular_flows):
        
        total_capacity = 0
        
        for (entry_id, lanes) in enumerate(topology.entry_lanescount):
            #print ('HCM2010 - Capacity of entry {} ({} lane(s))'.format(entry_id, lanes))
            
            upstream_flow = circular_flows['Flow{}'.format(entry_id)]
            #print ('upstream flow: {}'.format(upstream_flow))
            
            lane_capacity = 0
            
            if lanes == 1:
                lane_capacity = 1130 * np.exp(-0.001 * upstream_flow)
            if lanes == 2:
                lane_capacity = 1130 * (np.exp(-0.0007 * upstream_flow) + np.exp(-0.00075 * upstream_flow))
            
            total_capacity += lane_capacity
        
        return total_capacity
    
    
    @staticmethod
    def rd_capacity_hcm2016(topology, circular_flows):
        
        circulating_lanes = topology.real_lanes_count
        
        total_capacity = 0
        
        for (entry_id, lanes) in enumerate(topology.entry_lanescount):
            #print ('HCM2016 - Capacity of entry {} ({} lane(s))'.format(entry_id, lanes))
            
            upstream_flow = circular_flows['Flow{}'.format(entry_id)]
            #print ('upstream flow: {}'.format(upstream_flow))
            
            lane_capacity = 0
            
            if lanes == 1:
                
                if circulating_lanes == 1:
                    lane_capacity = 1380 * np.exp(-0.00102 * upstream_flow)
                elif circulating_lanes == 2:
                    lane_capacity = 1420 * np.exp(-0.00085 * upstream_flow)
                
            elif lanes == 2:
                
                if circulating_lanes == 1:
                    lane_capacity = 2.0 * 1420 * np.exp(-0.00091 * upstream_flow)
                elif circulating_lanes == 2:
                    lane_capacity = 1420 * np.exp(-0.00085 * upstream_flow) + 1350 * np.exp(-0.00092 * upstream_flow)
            
            total_capacity += lane_capacity
        
        return total_capacity
    
    '''
    @staticmethod
    def rd_capacity_german_easy(topology, circular_flows):
        
        Tf = 2.88
        Tc = 4.12
        
        total_capacity = 0
        for (entry_id, lanes) in enumerate(topology.entry_lanescount):
            #print ('German easy - Capacity of entry {} ({} lane(s))'.format(entry_id, lanes))
            
            upstream_flow = circular_flows['Flow{}'.format(entry_id)]
            #print ('upstream flow: {}'.format(upstream_flow))
            
            ne = None
            if lanes == 1:
                ne = 1.0
            elif lanes == 2:
                ne = 1.4
            
            total_capacity += 3600.0 * ne/Tf * np.exp((-upstream_flow/3600.0) * (Tc - Tf/2))
        
        return total_capacity
    '''
    
    @staticmethod
    def rd_capacity_german(topology, circular_flows):
        
        Tf = 2.88
        Tc = 4.12
        T0 = (Tc - Tf/2)
        headway = 2.10
        
        nc = topology.real_lanes_count
        
        total_capacity = 0
        for (entry_id, lanes) in enumerate(topology.entry_lanescount):
            #print ('German easy - Capacity of entry {} ({} lane(s))'.format(entry_id, lanes))
            
            upstream_flow_vps = circular_flows['Flow{}'.format(entry_id)] / 3600.0
            #print ('upstream flow: {}'.format(upstream_flow))
            
            ne = lanes
            lane_capacity = ((1 - (headway*upstream_flow_vps)/nc)**nc) * ne/Tf * np.exp(-upstream_flow_vps * (T0 - headway))
            
            total_capacity += lane_capacity
        
        return 3600.0 * total_capacity #back to veh/h
