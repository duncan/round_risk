#!/usr/bin/env python3

import pickle
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

# --------------------------- #
# 1. Training data formatting #
# --------------------------- #

training_data = np.array([[ 0.66666667,  0.50947082,  0.43835784, 0. ], #input 1 ['normalized_lateral_position', 'heading', 'normalized_distance_to_exit', 'did the vehicle exit?']
                          [ 0.,         -36.28659227, 0.12468592, 1. ], #input 2 ...
                          [ 0.33333333, -34.44893003, 0.19273091, 1. ], # ...
                          [ 0.33333333, -7.56013519,  0.46956706, 1. ],
                          [ 0.33333333, -1.38554593,  0.22985882, 0. ]])
np.random.shuffle(training_data)


print ("Training samples: {}".format(len(training_data)))

# Training & Validation data split
train, validation = train_test_split(training_data, test_size = 0.2)

# Separation of features and the target class (whether the vehicle exited)
(x_training, y_training, x_validation, y_validation) = (train[:,0:3], train[:,3], validation[:,0:3], validation[:,3])


# ---------------------- #
# 2. Logistic regression #
# ---------------------- #

model = LogisticRegression()
model.fit(x_training, y_training)

# Score the model accuracy
accuracy = model.score(x_validation, y_validation)  
print("Accuracy: {}".format(accuracy))

# Optionally, export the model as a pickle file.
pickle.dump(model, open("model.pickle", "wb"))
