#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import sys
import math
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

from PIL import Image
import numpy as np
import pandas as pd
import seaborn as sn
from scipy import stats
import pickle
import argparse
import json

import tools.read_csv_round as rd
import tools.consts as consts
from tools.topology import Topology, Lane
from tools.ttc_correlation import TTCTimeline, TTCData, VariationDataset
import tools.exit_model as exit_model_lib
import tools.locations as locations


def get_frames(tracks_meta, trackId):
    return (tracks_meta[trackId][rd.INITIAL_FRAME], tracks_meta[trackId][rd.FINAL_FRAME])

def read_frames(tracks, tracks_meta, last_frame=-1, last_track=-1):
    
    res = {}
    for frameId in range(last_frame+1):
        res[frameId] = []
        
        for trackId in range(last_track+1):
            frames = get_frames(tracks_meta, trackId)
            if frameId >= frames[0] and frameId <= frames[1]: # The object exists in that frame
                ix = list(tracks[trackId][consts.FRAME]).index(frameId)
                pos = tracks[trackId][consts.BBOX][ix]
                res[frameId].append({consts.TRACK_ID: trackId,
                                     consts.X: pos[0], consts.Y: pos[1],
                                     consts.WIDTH: pos[2], consts.HEIGHT: pos[3],
                                     consts.HEADING: tracks[trackId][consts.HEADING][ix],
                                     consts.X_VELOCITY: tracks[trackId][consts.X_VELOCITY][ix],
                                     consts.Y_VELOCITY: tracks[trackId][consts.Y_VELOCITY][ix]})
    return res



# .......................................................................... #
# Browse all recordings associated with a given location to compute TTC data #
# The obtained ttc data is saved into a pickle file to be reused by          #
# analysis.py .............................................................. #
# .......................................................................... #


print ("Loading recordings for location 0")
input_ids = [sys.argv[1]]

print ("Input files for location {}: {}".format(0, input_ids))


input_args = []

for input_id in input_ids:
    input_args.append({"id": input_id, "input_path": consts.ROUND_PATH+input_id+"_tracks.csv",
                       "input_static_path": consts.ROUND_PATH+input_id+"_tracksMeta.csv",
                       "input_meta_path": consts.ROUND_PATH+input_id+"_recordingMeta.csv" })


# Loading topology for the location.
topology = locations.get_topology_for_location(0)

# .................................................................................... #
# If requested, compute exiting probability estimation models to weight TTC risk with. #
# .................................................................................... #

exit_proba_models = {}
'''if argsparse.probability_weighting:
    print("Computing exit probability models...")
    
    for input_str in input_ids:
        model_training_inputs = input_ids_all.copy()
        model_training_inputs.remove(input_str) #Do not train the model using the data it will be applied to.
        
        (exit_model, accuracy) = exit_model_lib.get_exit_proba_model(model_training_inputs, 101010, interaction=False)
        print ("Model trained excluding {}_tracks, accuracy: {}".format(input_str, accuracy))
        print ('{} -> {}'.format(input_str, model_training_inputs))
        
        exit_proba_models[input_str] = exit_model
'''

np.random.seed(42)

args = input_args[0]

tracks = rd.read_track_csv(args)
tracks_meta = rd.read_static_info(args)
recordings_meta = rd.read_meta_info(args)

last_frame = int(recordings_meta[rd.FRAME_RATE] * recordings_meta[rd.DURATION]) - 1
last_track = int(recordings_meta[rd.NUM_TRACKS]) - 1

print ("Read tracks: {}".format(args['input_path']))
print ("Nb tracks:{}, Last Frame: {}".format(last_track, last_frame))

print ("Loading frames...")
frames = read_frames(tracks, tracks_meta, last_frame=last_frame, last_track=last_track)

result = pd.DataFrame(columns=['frame', 'rear', 'exit_rear', 'front', 'exit_front', 'ttc', 'heading_rear', 'distance_rear', 'lateral_pos_rear'])

tracked_vehicles = []
exit_data = {}

# A. TTC VALUES COMPUTATION
for frameId in range(len(frames)):
    print("frame {}/{}".format(frameId, last_frame), end='\r')
    for obj in frames[frameId]:
        
        #Fetch TTC data
        ttc = topology.getTTC(frames[frameId], obj, [0])
        
        #Add TTC data
        if ttc != None and ttc[1][0][1] > 0:
            vfront = ttc[0]
            
            if ttc[1][0][1] < 6:
                
                tracked_vehicles.append(obj[consts.TRACK_ID])
                
                # 1. current lane id
                current_lane = topology.get_lane_distance(obj)
                
                # 2. relative heading
                signed_relheading = topology.get_relative_heading(obj)
                
                # 3. straight-line distance to next exit.
                (next_exit_id, distance, _) = topology.get_distance_to_next_exit(obj)
                
                result = result.append({'frame':frameId, 'rear':obj[consts.TRACK_ID], 'exit_rear':next_exit_id,
                                        'front':vfront[consts.TRACK_ID], 'exit_front':topology.getnextexit(vfront),
                                        'ttc':ttc[1][0][1], 'heading_rear':signed_relheading, 'distance_rear':distance,
                                        'lateral_pos_rear':current_lane}, ignore_index=True)
        
        if obj[consts.TRACK_ID] in tracked_vehicles and obj[consts.TRACK_ID] not in exit_data:
            eval_exit = topology.getobjectexits(obj)
            if eval_exit != -1:
                exit_data[obj[consts.TRACK_ID]] = eval_exit



def add_exit_col(vid):
    if vid in exit_data:
        return exit_data[vid]
    else:
        return -1

result['real_rear_exit'] = result['rear'].apply(add_exit_col)

result.to_csv('false_positives/{}.csv'.format(args['id']))
