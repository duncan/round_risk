#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import time
import numpy as np
import scipy.stats
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from EDGE.EDGE_4_4_1 import EDGE

import sys
sys.path.append( '..' )

import tools.consts as consts
import tools.exit_model as model
from tools.locations import get_input_interaction, get_input_for_location

ROUND_PATH = "/home/scooter/round_risk/path/to/round/data/" # TODO: Replace with the path to the RounD dataset
INTER_PATH = "/home/scooter/round_risk/interaction/interaction/recorded_trackfiles"

DATASET_SIZE = 10000

def entropy(labels, base=2):
  value, counts = np.unique(labels, return_counts=True)
  print (value)
  #print (counts)
  return scipy.stats.entropy(counts, base=base)

# 1. Training classifiers.
randseed = int(time.time())
print ('\nUsing seed {}...'.format(randseed))

roundabouts = {'INT_USA_FT': {'interaction': True,  'input_raw': get_input_interaction('DR_USA_Roundabout_FT'), 'name': 'DR_USA_Roundabout_FT'},
               'INT_USA_SR': {'interaction': True,  'input_raw': get_input_interaction('DR_USA_Roundabout_SR'), 'name': 'DR_USA_Roundabout_SR'},
               'INT_USA_EP': {'interaction': True,  'input_raw': get_input_interaction('DR_USA_Roundabout_EP'), 'name': 'DR_USA_Roundabout_EP'},
               'INT_CHN_LN': {'interaction': True,  'input_raw': get_input_interaction('DR_CHN_Roundabout_LN'), 'name': 'DR_CHN_Roundabout_LN'},
               'INT_DEU_OF': {'interaction': True,  'input_raw': get_input_interaction('DR_DEU_Roundabout_OF'), 'name': 'DR_DEU_Roundabout_OF'},
               'RD_0':       {'interaction': False, 'input': get_input_for_location(0)},
               'RD_1':       {'interaction': False, 'input': get_input_for_location(1)},
               'RD_2':       {'interaction': False, 'input': get_input_for_location(2)}}

roundabouts_data = {}

def prepare_data(roundabouts, key):
    
     # Extract validation and training sets.
    (x_training, y_training, x_validation, y_validation) = model.process_training_data(roundabouts[key]['data'])
    print('{}: len training: {}, valid: {}'.format(key, len(y_training), len(y_validation)))
    
    if len(y_validation) < 100:
        del roundabouts[key]
        return
    
    roundabouts[key]['validation'] = (x_validation[0:DATASET_SIZE//2], y_validation[0:DATASET_SIZE//2])
    
    # Perform regression
    regression = LogisticRegression()
    regression.fit(x_training, y_training)
    
    #print (regression.classes_)
    true_class = [i for i in range(len(regression.classes_)) if regression.classes_[i] == True][0]
    #print (true_class)
    
    accuracy = regression.score(x_validation, y_validation)  
    print("{} accuracy: {}".format(key, accuracy))
    
    roundabouts[key]['classifier'] = regression

for key in roundabouts:
    # Preprocess paths for interaction roundabouts
    if roundabouts[key]['interaction']:
        
        roundabouts[key]['input'] = []
        for value in roundabouts[key]['input_raw']:
            roundabouts[key]['input'].append( roundabouts[key]['name']+'_'+os.path.basename(value) )
        
        del roundabouts[key]['input_raw']
        del roundabouts[key]['name']
    
    # Extract training data
    basepath = '../'
    if roundabouts[key]['interaction']:
        basepath += 'interaction/'
    
    data = np.array(model.gather_training_data(roundabouts[key]['input'],
                                               randseed,
                                               roundabouts[key]['interaction'],
                                               basepath, filterout=False))
    
    # Normalize training data
    data = np.delete(data, 2, axis=1) # Remove absolute distance data
    data[:,0] /= data[:,0].max()
    
    df = pd.DataFrame(data=data, index=None, columns=['Lane',
                                                      'Heading',
                                                      'DistanceRel',
                                                      'MeanApproachSpeed',
                                                      'MeanDensity',
                                                      'Flow',
                                                      'Capacity_German',
                                                      'Capacity_HCM2010',
                                                      'FOC_German',
                                                      'FOC_HCM2010',
                                                      'NextExit'])
    
    training_cols = ['Lane', 'Heading', 'DistanceRel', 'NextExit']
    
    traffic_ranges = [(0.0, 0.1), (0.1, 0.2), (0.2, 0.3), (0.3, 0.4), (0.4, 0.5), (0.5, 0.6)]
    for (tmin, tmax) in traffic_ranges:
        df_traffic = df[(df['FOC_German'] >= tmin) & (df['FOC_German'] < tmax)]
        if df_traffic.shape[0] >= 5000: # tolerate 5000 entries
            df_traffic = df_traffic[training_cols]
            roundabouts_data['{}-{}_{}'.format(key, tmin, tmax)] = {'data': df_traffic.to_numpy()}
            prepare_data(roundabouts_data, '{}-{}_{}'.format(key, tmin, tmax))


# 2. Computing MI for all pairs of roundabouts
def classify(items, classifier):
    res = []
    
    for item in items:
        sample = np.array([item])
        proba = classifier.predict_proba(sample)
        res.append([np.round(proba[0][1],1)])
    
    return np.array(res)


r_names = list(roundabouts_data.keys())
mi_matrix = pd.DataFrame(columns=r_names, index=r_names)

''' overall evaluation set
evaluation_set = np.array(roundabouts_data[r_names[0]]['validation'][0])
for i in range(1, len(r_names)):
    evaluation_set = np.append(evaluation_set, roundabouts_data[r_names[i]]['validation'][0], axis=0)

np.random.shuffle(evaluation_set)
evaluation_set = evaluation_set[0:DATASET_SIZE]
'''

for i in range(len(r_names)):
    for j in range(i+1, len(r_names)):
        key1, key2 = r_names[i], r_names[j]
        print ('{} / {}'.format(key1, key2))
        
        evaluation_set = np.append(roundabouts_data[key1]['validation'][0],
                                   roundabouts_data[key2]['validation'][0], axis=0)
        print (evaluation_set.shape)
        
        classif1 = classify(evaluation_set, roundabouts_data[key1]['classifier'])
        classif2 = classify(evaluation_set, roundabouts_data[key2]['classifier'])
        
        mi = EDGE(classif1, classif2, gamma=[0.1, 0.1])
        entrop1 = entropy(classif1)
        entrop2 = entropy(classif2)
        
        #print (mi)
        mi_matrix.at[key1,key2] = mi/entrop1
        mi_matrix.at[key2,key1] = mi/entrop2
        

mi_matrix.to_json('mi_traffic/{}.json'.format(randseed))
print (mi_matrix)
