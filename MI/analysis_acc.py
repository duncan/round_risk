#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import time
import argparse
import numpy as np
import pandas as pd
from pandas.plotting import table 
import matplotlib.pyplot as plt
from scipy import stats
from dtreeviz.trees import dtreeviz
import six
import seaborn as sn

import sklearn.tree as tree
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split

import sys
sys.path.append( '..' )

from tools.locations import get_topology_interaction, get_topology_for_location
from tools.topology import Topology


def geometry_df():
    roundabouts = {'INT_USA_FT': {'COUNTRY': 'USA', 'interaction': True, 'name': 'DR_USA_Roundabout_FT'},
                   'INT_USA_SR': {'COUNTRY': 'USA', 'interaction': True, 'name': 'DR_USA_Roundabout_SR'},
                   'INT_USA_EP': {'COUNTRY': 'USA', 'interaction': True, 'name': 'DR_USA_Roundabout_EP'},
                   'INT_CHN_LN': {'COUNTRY': 'CHN', 'interaction': True, 'name': 'DR_CHN_Roundabout_LN'},
                   'INT_DEU_OF': {'COUNTRY': 'DEU', 'interaction': True, 'name': 'DR_DEU_Roundabout_OF'},
                   'RD_0':       {'COUNTRY': 'DEU', 'interaction': False, 'name': 0},
                   'RD_1':       {'COUNTRY': 'DEU', 'interaction': False, 'name': 1},
                   'RD_2':       {'COUNTRY': 'DEU', 'interaction': False, 'name': 2}}

    for key in roundabouts:
        topology = None
        if roundabouts[key]['interaction']:
            topology = get_topology_interaction(roundabouts[key]['name'])
        else:
            topology = get_topology_for_location(roundabouts[key]['name'])
        
        roundabouts[key]['ENTRIES_COUNT'] = len(topology.entry_lanescount)
        roundabouts[key]['CIRCULAR_LANES_COUNT'] = topology.real_lanes_count
        roundabouts[key]['RADIUS'] = topology.circular_lanes[0].radius_begin
        roundabouts[key]['WIDTH'] = topology.circular_lanes[-1].radius_end - roundabouts[key]['RADIUS']
        
        del roundabouts[key]['interaction']
        del roundabouts[key]['name']

    return pd.DataFrame.from_dict(roundabouts, orient='index')


parser = argparse.ArgumentParser()
parser.add_argument("--training_size", help="Size of training sets.", type=int)
argsparse = parser.parse_args()


geo_df = geometry_df()
geo_df.to_csv('geometry.csv')

print (geo_df)

# Loading mi matrix data
mi = []
inputs = os.listdir('mi_data/mi_f1_{}'.format(argsparse.training_size))
for i in inputs:
    mi.append(pd.read_json('mi_data/mi_f1_{}/{}'.format(argsparse.training_size, i)))

if len(mi) == 0:
    sys.exit('No input file located.')


# Computing means and confidence intervals
t = stats.t.ppf(0.95, len(mi)-1)
r_names = list(mi[0])
r_names.remove('SHUFFLED')

features = []
#for f in geo_df.columns:
#    features.append('{}_1'.format(f))
for f in geo_df.columns:
    if f == 'COUNTRY':
        features.append('SAME_COUNTRY')
    else:
        features.append('{}_DIFF'.format(f))
features.append('UC')

training_set = pd.DataFrame(columns=features)

mi_matrix = pd.DataFrame(columns=r_names, index=r_names)

def training_row(rd_features, key1, key2, mi_matrix):
    training_row = []
    temp = []
    
    for col in rd_features:
        temp.append(geo_df.at[key1,col])
    for (ix, col) in enumerate(rd_features):
        if ix == 0: #country
            training_row.append(geo_df.at[key2,col] == temp[ix])
        else:
            training_row.append(np.abs(geo_df.at[key2,col] - temp[ix]))
    print (training_row)
    training_row.append(mi_matrix.at[key2,key1])
    return training_row

for i in range(len(r_names)):
    for j in range(i+1, len(r_names)):
        key1, key2 = r_names[i], r_names[j]
        
        values12, values21, values11, values22 = [], [], [], []
        for matrix in mi:
            values12.append(matrix.at[key1,key2])
            values11.append(matrix.at[key1,key1])
            values21.append(matrix.at[key2,key1])
            values22.append(matrix.at[key2,key2])
        
        values12 = np.array(values12)
        values21 = np.array(values21)
        values22 = np.array(values22)
        values11 = np.array(values11)
        
        mi_matrix.at[key1,key2] = np.mean(values12)
        mi_matrix.at[key2,key1] = np.mean(values21)
        mi_matrix.at[key1,key1] = np.mean(values11)
        
        training_set.loc['{}/{}'.format(key1,key2)] = training_row(geo_df.columns, key1, key2, mi_matrix)
        training_set.loc['{}/{}'.format(key2,key1)] = training_row(geo_df.columns, key2, key1, mi_matrix)
        training_set.loc['{}/{}'.format(key2,key2)] = training_row(geo_df.columns, key1, key1, mi_matrix)


plot_matrix = mi_matrix[mi_matrix.columns].astype(float)
chart = sn.heatmap(plot_matrix, annot=True, cmap="YlGnBu", mask=plot_matrix.isnull())
chart.set_xticklabels(chart.get_xticklabels(), rotation=20)
chart.set_yticklabels(chart.get_yticklabels(), rotation=20)
plt.show()

#print (mi_matrix)

#train, test = train_test_split(training_set, test_size=0.01)
train = training_set
print (train)
train.to_csv('training_set.csv')

clf = DecisionTreeRegressor(max_depth = 4, 
                             random_state = 0)

features_nolabel = features[0:len(features)-1]
training_x, training_y = train[features_nolabel], train['UC']
#test_x, test_y = test[features_nolabel], test['UC']

clf.fit(training_x, training_y)
#print ('accuracy: {}'.format(clf.score(test_x, test_y)))

plt.figure(figsize=(12,12))
tree.plot_tree(clf, filled=True, feature_names=features_nolabel);
plt.show()

viz = dtreeviz(clf, training_x, training_y,
                target_name="proficiency",
                feature_names=features_nolabel)

viz.view()

train = train.apply(pd.to_numeric)
print(train)
corrMatrix = train.corr()
sn.heatmap(corrMatrix, annot=True)
plt.show()
