#!/usr/bin/env python3
'''
Author: Duncan Deveaux
'''

import os
import time
import argparse
import numpy as np
import pandas as pd
from pandas.plotting import table

import matplotlib.pyplot as plt
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42
plt.rcParams['figure.figsize'] = (11, 8)

from scipy import stats
import seaborn as sns
from dtreeviz.trees import dtreeviz
import six
import seaborn as sn
import pickle


from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score

import sys
sys.path.append( '..' )

from tools.locations import get_topology_interaction, get_topology_for_location
from tools.topology import Topology

import tools.consts as consts
import tools.exit_model as model
from tools.locations import get_input_interaction, get_input_for_location

def geometry_df():
    roundabouts = {'INT_USA_FT': {'COUNTRY': 'USA', 'interaction': True, 'name': 'DR_USA_Roundabout_FT'},
                   'INT_USA_SR': {'COUNTRY': 'USA', 'interaction': True, 'name': 'DR_USA_Roundabout_SR'},
                   'INT_USA_EP': {'COUNTRY': 'USA', 'interaction': True, 'name': 'DR_USA_Roundabout_EP'},
                   'INT_CHN_LN': {'COUNTRY': 'CHN', 'interaction': True, 'name': 'DR_CHN_Roundabout_LN'},
                   'INT_DEU_OF': {'COUNTRY': 'DEU', 'interaction': True, 'name': 'DR_DEU_Roundabout_OF'},
                   'RD_0':       {'COUNTRY': 'DEU', 'interaction': False, 'name': 0},
                   'RD_1':       {'COUNTRY': 'DEU', 'interaction': False, 'name': 1},
                   'RD_2':       {'COUNTRY': 'DEU', 'interaction': False, 'name': 2}}

    for key in roundabouts:
        topology = None
        if roundabouts[key]['interaction']:
            topology = get_topology_interaction(roundabouts[key]['name'])
        else:
            topology = get_topology_for_location(roundabouts[key]['name'])
        
        roundabouts[key]['ENTRIES_COUNT'] = len(topology.entry_lanescount)
        roundabouts[key]['CIRCULAR_LANES_COUNT'] = topology.real_lanes_count
        roundabouts[key]['RADIUS'] = topology.circular_lanes[0].radius_begin
        roundabouts[key]['WIDTH'] = topology.circular_lanes[-1].radius_end - roundabouts[key]['RADIUS']
        
        del roundabouts[key]['interaction']
        del roundabouts[key]['name']

    return pd.DataFrame.from_dict(roundabouts, orient='index')


# Loading geometry data
geo_df = geometry_df()


# Training models
TRAINING_SIZE = 5000
EVALUATION_SIZE = 1000
NB_MODELS_CONFIDENCE = 20 #NB_MODELS_CONFIDENCE = 20


roundabouts = {'INT_USA_FT': {'interaction': True,  'input_raw': get_input_interaction('DR_USA_Roundabout_FT'), 'name': 'DR_USA_Roundabout_FT'},
               'INT_USA_SR': {'interaction': True,  'input_raw': get_input_interaction('DR_USA_Roundabout_SR'), 'name': 'DR_USA_Roundabout_SR'},
               'INT_USA_EP': {'interaction': True,  'input_raw': get_input_interaction('DR_USA_Roundabout_EP'), 'name': 'DR_USA_Roundabout_EP'},
               'INT_CHN_LN': {'interaction': True,  'input_raw': get_input_interaction('DR_CHN_Roundabout_LN'), 'name': 'DR_CHN_Roundabout_LN'},
               'INT_DEU_OF': {'interaction': True,  'input_raw': get_input_interaction('DR_DEU_Roundabout_OF'), 'name': 'DR_DEU_Roundabout_OF'},
               'RD_0':       {'interaction': False, 'input': get_input_for_location(0)},
               'RD_1':       {'interaction': False, 'input': get_input_for_location(1)},
               'RD_2':       {'interaction': False, 'input': get_input_for_location(2)}}


ROUND_PATH = "/home/scooter/round_risk/path/to/round/data/" # TODO: Replace with the path to the RounD dataset
INTER_PATH = "/home/scooter/round_risk/interaction/interaction/recorded_trackfiles"

'''
def train_classifier(roundabouts, key, training_size, evaluation_size):
     # Extract validation and training sets.
    (x_training, y_training, x_validation, y_validation) = model.process_training_data(roundabouts[key]['data'], training_size, evaluation_size)
    print('{}: len training: {}, valid: {}'.format(key, len(y_training), len(y_validation)))
    
    if len(y_validation) < 100:
        del roundabouts[key]
        return
    
    roundabouts[key]['validation'] = (x_validation, y_validation)
    
    # Perform regression
    regression = LogisticRegression()
    regression.fit(x_training, y_training)
    
    #print (regression.classes_)
    true_class = [i for i in range(len(regression.classes_)) if regression.classes_[i] == True][0]
    #print (true_class)
    
    accuracy = regression.score(x_validation, y_validation)  
    print("{} accuracy: {}".format(key, accuracy))
    
    roundabouts[key]['classifier'] = regression
'''

def train_classifiers(nb_classifiers, training_data, training_size, evaluation_size):
    
    classifs = []
    validations = []
    
    for i in range(nb_classifiers):
        
        # Shuffle training data
        np.random.shuffle(training_data)
        
        # Extract training & validation data
        (x_training, y_training, x_validation, y_validation) = model.process_training_data(training_data, training_size, evaluation_size)
        if len(y_validation) < 100:
            raise ValueError('len(y_validation) < 100')
        
        validations.append((x_validation, y_validation))
        
        # Perform regression
        regression = LogisticRegression()
        regression.fit(x_training, y_training)
        
        classifs.append(regression)
        
    return (classifs, validations)

roundabouts_data = {}

'''
for key in roundabouts:
    # Preprocess paths for interaction roundabouts
    if roundabouts[key]['interaction']:
        
        roundabouts[key]['input'] = []
        for value in roundabouts[key]['input_raw']:
            roundabouts[key]['input'].append( roundabouts[key]['name']+'_'+os.path.basename(value) )
        
        del roundabouts[key]['input_raw']
        del roundabouts[key]['name']
    
    # Extract training data
    basepath = '../'
    if roundabouts[key]['interaction']:
        basepath += 'interaction/'
    
    data = np.array(model.gather_training_data(roundabouts[key]['input'],
                                               -1,
                                               roundabouts[key]['interaction'],
                                               basepath, filterout=False))
    
    # Normalize training data
    data = np.delete(data, 2, axis=1) # Remove absolute distance data
    data[:,0] /= data[:,0].max()
    
    df = pd.DataFrame(data=data, index=None, columns=['Lane',
                                                      'Heading',
                                                      'DistanceRel',
                                                      'MeanApproachSpeed',
                                                      'MeanDensity',
                                                      'Flow',
                                                      'Capacity_German',
                                                      'Capacity_HCM2010',
                                                      'FOC_German',
                                                      'FOC_HCM2010',
                                                      'NextExit'])
    
    training_cols = ['Lane', 'Heading', 'DistanceRel', 'NextExit']
    
    data_np = df[training_cols].to_numpy()
    
    (classifs, validations) = train_classifiers(NB_MODELS_CONFIDENCE, data_np, TRAINING_SIZE, EVALUATION_SIZE)
    roundabouts_data[key] = {'classifiers':classifs, 'valids': validations}

# Save / load roundabouts_data
with open("voting_classifiers.pickle", "wb") as f:
    pickle.dump(roundabouts_data, f)
'''

with open("voting_classifiers.pickle", "rb") as f:
    roundabouts_data = pickle.load(f)


def classify(items, classifier):
    res = []
    
    for item in items:
        sample = np.array([item])
        proba = classifier.predict_proba(sample)
        res.append([proba[0][1]])
    
    return np.array(res)

'''Predict whether vehicles will exit or not (boolean) based on exit probabilities.'''
def predict(probas):
    predictions = []
    for proba in probas:
        if proba > 0.5:
            predictions.append(1.0)
        else:
            predictions.append(0.0)
    
    return np.array(predictions)

def compute_confidence(dataset):
    
    n = len(dataset)
    t = stats.t.ppf(0.95, n-1)
    stdev = np.std(dataset)
    
    return (np.mean(dataset), t*stdev/np.sqrt(n))

def score_models(validation_target, classifs_sources):
    evaluation_set = validation_target[0]
    ground_truth = validation_target[1]
    
    classifs = []
    for classif in classifs_sources:
        classifs.append(classify(evaluation_set, classif)[:,0])
    
    predictions = predict(np.mean(classifs, axis=0))
    
    return {'acc': accuracy_score(predictions, ground_truth),
            'f1': f1_score(predictions, ground_truth),
            'precision': precision_score(predictions, ground_truth),
            'recall': recall_score(predictions, ground_truth)}

def score_models_with_confidence(rds, key_target, keys_classifiers):
    
    items = {'acc':[], 'f1':[], 'precision':[], 'recall':[]}
    
    for i in range(NB_MODELS_CONFIDENCE):
        
        validation_target = rds[key_target]['valids'][i]
        classifs_sources = []
        
        for key_c in keys_classifiers:
            classifs_sources.append(rds[key_c]['classifiers'][i])
        
        one_score = score_models(validation_target, classifs_sources)
        items['acc'].append(one_score['acc'])
        items['f1'].append(one_score['f1'])
        items['precision'].append(one_score['precision'])
        items['recall'].append(one_score['recall'])
    
    #print (items)
    
    (acc_mean, acc_confidence) = compute_confidence(items['acc'])
    (f1_mean, f1_confidence) = compute_confidence(items['f1'])
    (precision_mean, precision_confidence) = compute_confidence(items['precision'])
    (recall_mean, recall_confidence) = compute_confidence(items['recall'])
    
    res = {'acc':       {'mean': acc_mean,       'error': acc_confidence},
           'f1':        {'mean': f1_mean,        'error': f1_confidence},
           'precision': {'mean': precision_mean, 'error': precision_confidence},
           'recall':    {'mean': recall_mean,    'error': recall_confidence}}
    
    return res

# ------------- #
# PLOTTING PART #
# ------------- #

def get_similarity_groups(geometry, key):
    entries = geometry.at[key, 'ENTRIES_COUNT']
    country = geometry.at[key, 'COUNTRY']
    radius = geometry.at[key, 'RADIUS']
    width = geometry.at[key, 'WIDTH']
    
    df = geometry.drop(key)
    df['ENTRIES_DIFF'] = (df['ENTRIES_COUNT'] - entries).abs()
    df['RADIUS_DIFF'] = (df['RADIUS'] - radius).abs()
    df['WIDTH_DIFF'] = (df['WIDTH'] - width).abs()
    df['SAME_COUNTRY'] = (df['COUNTRY'] == country)
    
    df_similar = df[(df['ENTRIES_DIFF'] < 1) & (df['RADIUS_DIFF'] <= 6.0) ]
    #df_similar = pd.concat( [df[(df['ENTRIES_DIFF'] < 1) & (df['RADIUS_DIFF'] < 8.12)], df[(df['ENTRIES_DIFF'] >= 1) & (df['WIDTH_DIFF'] < 1.125)]] ).drop_duplicates()
    df_distant = pd.concat([df,df_similar]).drop_duplicates(keep=False)
    
    return (df_similar, df_distant)


def get_accuracies(similar, distant, targetkey, metric, rds):
    res = {'elements_similar':{}, 'elements_distant':{}}
    
    others = []
    for key1 in similar.index:
        others.append(key1)
        score = score_models_with_confidence(rds, targetkey, [key1])
        res['elements_similar'][key1] = score[metric]
        
    for key2 in distant.index:
        others.append(key2)
        score = score_models_with_confidence(rds, targetkey, [key2])
        res['elements_distant'][key2] = score[metric]
    
    if len(similar) > 0:
        res['similar'] = score_models_with_confidence(rds, targetkey, similar.index)[metric]
    
    if len(distant) > 0:
        res['distant'] = score_models_with_confidence(rds, targetkey, distant.index)[metric]
    
    res['others'] = score_models_with_confidence(rds, targetkey, others)[metric]
        
    return res

#(sim, dist) = get_similarity_groups(geo_df, 'INT_CHN_LN')
#print ('sim: {} / dist: {}'.format(sim,dist))

#print (get_accuracies(sim, dist, 'INT_CHN_LN', 'acc', roundabouts_data))

def reformat_index(item):
    if item == 'RD_0':
        return 'RounD_0'
    elif item == 'RD_1':
        return 'RounD_1'
    elif item == 'RD_2':
        return 'RounD_2'
    elif item.startswith('INT_'):
        return item[4:]

def plot_scatter(geo_df, metric, rds, voting=True):
    
    mrk_dots = '.'
    mrk_voting = 'p'
    
    x = 0.0
    xticks = []
    xticks_labels = []
    for targetkey in geo_df.index:
        
        (sim, dist) = get_similarity_groups(geo_df, targetkey)
        accs = get_accuracies(sim, dist, targetkey, metric, rds)
        
        similar_accs, similar_accs_error = [], []
        distant_accs, distant_accs_error = [], []
        
        score_target = score_models_with_confidence(rds, targetkey, [targetkey])[metric]
        acc_target = score_target['mean']
        acc_target_error = score_target['error']
        
        
        if len(sim) > 0:
            similar_accs = [accs['elements_similar'][val]['mean'] for val in accs['elements_similar']]
            similar_accs_error = [accs['elements_similar'][val]['error'] for val in accs['elements_similar']]
            #print (similar_accs_error)
            
            print ('{}: voting sim accs: {}'.format(targetkey, accs['similar']['mean']))
            
            if int(x) == len(geo_df.index) - 1:
                plt.errorbar(np.ones(len(similar_accs))*(x-0.1), similar_accs, yerr=similar_accs_error, color='blue', label='Similar roundabouts', marker=mrk_dots, fmt='o')
                if voting:
                    plt.errorbar([x], accs['similar']['mean'], yerr=accs['similar']['error'], color='cornflowerblue', label='Voting of similar roundabouts', marker=mrk_voting, fmt='o')
            else:
                plt.errorbar(np.ones(len(similar_accs))*(x-0.1), similar_accs, yerr=similar_accs_error, color='blue', marker=mrk_dots, fmt='o')
                if voting:
                    plt.errorbar([x], accs['similar']['mean'], yerr=accs['similar']['error'], color='cornflowerblue', marker=mrk_voting, fmt='o')
        
        if len(dist) > 0:
            distant_accs = [accs['elements_distant'][val]['mean'] for val in accs['elements_distant']]
            distant_accs_error = [accs['elements_distant'][val]['error'] for val in accs['elements_distant']]
            #print (distant_accs_error)
            
            print ('{}: dist accs: {}'.format(targetkey, distant_accs))
            
            if int(x) == len(geo_df.index) - 1:
                plt.errorbar(np.ones(len(distant_accs))*(x+0.1), distant_accs, yerr=distant_accs_error, color='red', label='Distant roundabouts', marker=mrk_dots, fmt='o')
                if voting:
                    plt.errorbar([x], accs['distant']['mean'], yerr=accs['distant']['error'], color='indianred', label='Voting of distant roundabouts', marker=mrk_voting, fmt='o')
            else:
                plt.errorbar(np.ones(len(distant_accs))*(x+0.1), distant_accs, yerr=distant_accs_error, color='red', marker=mrk_dots, fmt='o')
                if voting:
                    plt.errorbar([x], accs['distant']['mean'], yerr=accs['distant']['error'], color='indianred', marker=mrk_voting, fmt='o')
        
        if int(x) == len(geo_df.index) - 1:
            if voting:
                plt.errorbar([x], accs['others']['mean'], yerr=accs['others']['error'], color='green', label='Voting of all except target', marker=mrk_voting, fmt='o')
            plt.errorbar([x], acc_target, yerr=acc_target_error, color='black', label='Target roundabout', marker='_', fmt='o')
        else:
            if voting:
                plt.errorbar([x], accs['others']['mean'], yerr=accs['others']['error'], color='green', marker=mrk_voting, fmt='o')
            plt.errorbar([x], acc_target, yerr=acc_target_error, color='black', marker='_', fmt='o')
        
        
        xticks.append(x)
        xticks_labels.append(reformat_index(targetkey))
        x += 1.0
        
        print ('{} ok'.format(targetkey))

    plt.suptitle('Accuracy Score of Exit Probability Models Trained and Applied on Different Roundabouts', y=0.935)
    plt.title('Similarity Condition: ΔEntries = 0 AND ΔRadius ≤ 8.12m', fontsize=11)
    plt.ylabel('Accuracy Score on the Target Model')
    plt.xlabel('Target Model')
    plt.xticks(xticks, xticks_labels)
    plt.legend(bbox_to_anchor=(0.5, -0.07), loc="upper center", ncol=3)
    plt.show()

plot_scatter(geo_df, 'acc', roundabouts_data, True)
